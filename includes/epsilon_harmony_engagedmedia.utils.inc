<?php

/**
 * @file
 * Useful functions and methods.
 */

/**
 * Gets the available webforms in the system.
 *
 * @return array
 *   An array containing the webforms.
 */
function ep_en_get_available_webforms() {
  $available_webforms = db_select('node', 'nd')
    ->fields('nd', array('title', 'nid'))
    ->condition('type', 'webform')
    ->execute()
    ->fetchAll();

  return $available_webforms;
}

/**
 * Gets the webforms that have the Wallet integration enabled.
 *
 * @return array
 *   An array containing the webforms.
 */
function ep_en_get_enabled_webforms() {
  $available_webforms = ep_en_get_available_webforms();
  $enabled_webforms = array();

  foreach ($available_webforms as $key => $value) {
    $form_enabled = variable_get('wallet_url_integration_enabled_' . $value->nid);
    if ($form_enabled) {
      $enabled_webforms[$value->nid] = $value->title;
    }
  }

  return $enabled_webforms;
}



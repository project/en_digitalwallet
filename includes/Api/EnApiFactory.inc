<?php

namespace Drupal\epsilon_harmony_engagemedia;

module_load_include('inc', 'epsilon_harmony_engagemedia', 'includes/Api/EnConnectionFactory');

use Drupal\epsilon_harmony\EpsilonApiFactory;
use Exception as Exception;

/**
 * Description for API Factory of Engagedmedia Wallet API.
 *
 * @author Sourav Konar
 */
class EnApiFactory extends EnConnectorFactory {

  /**
   * Send a real time email via Epsilon.
   *
   * @param string $message_id
   *   Message ID key to be passed for a particular template to trigger.
   * @param array $record
   *   Data to be passed through API.
   *
   * @return array
   *   Returns the response recieved from the API.
   */
  public function sendWalletEpsilonharmonyMail($toEmail = NULL, $webform_id = NULL, $attributes = array()) {

    $epclassObj = new EpsilonApiFactory;
    $record = [];
    //$record['subjectOverride'] = "<Override if necessary not a required attribute>";
    $attrib_arr = array();
    $message_key = variable_get('digital_wallet_webform_harmoney_key_' . $webform_id);
    $attrib_arr = $attributes;
    $record['recipients'][] = array(
      "customerKey" => $toEmail,
      "emailAddress" => $toEmail,
      'attributes' => $attrib_arr
    );
    $epclassObj->sendMessage($message_key, $record);
  }

  public function buildParameters($webform_id = NULL, $arr_attrib_val = array()) {
    $tokens = variable_get('digital_wallet_webform_dynamic_parameters_' . $webform_id);
    $tokens_array = list_extract_allowed_values($tokens, 'list_text', FALSE);
    $attrib_arr = array();
    foreach ($tokens_array as $key => $num) {
      $attrib_arr[$num - 1] = array(
        'attributeName' => $key,
        'attributeType' => "String",
        'attributeValue' => $arr_attrib_val[$num - 1]
      );
    }
    return $attrib_arr;
  }

  public function buildShortUrl($groupNumber = NULL, $card_number = NULL) {

    try {
      $med_code = $this->getMedcode();
      $request_url = $this->setApiEndpointUrl();
      $auth_code = 'Basic ' . $this->setEngagedmediaAuthToken();
      $query = array(
        'medCode' => $med_code,
        'memberID' => $card_number,
        'groupNumber' => $groupNumber,
      );
      $options = array(
        'headers' => array(
          'Authorization' => $auth_code
        ),
        'method' => 'GET',
      );
      $request_url = url($request_url, array('query' => $query));     
      $result = drupal_http_request($request_url, $options);      
      // Log the API.
      $logArr['endpoint'] = $request_url;
      $logArr['status_code'] = (isset($result->code)) ? $result->code : "";
      $logArr['status_message'] = (isset($result->status_message)) ? $result->status_message : "";
      $logArr['response'] = (isset($result->data)) ? $result->data : $result->error;
      en_digitalwallet_log($logArr);

      $short_url = '';
      if (isset($result) && $result->code == 200) {
        $short_url = json_decode($result->data)->link;
      }
      return $short_url;
    }
    catch (Exception $e) {
      watchdog_exception('epsilon_harmony_engagedmedia', $e, $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }

}

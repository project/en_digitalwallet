<?php

namespace Drupal\epsilon_harmony_engagemedia;

use Exception as Exception;

/**
 * Description for Connector Factory of Engagedmedia Wallet API.
 *
 * @author Sourav Konar
 */
class EnConnectorFactory {

  /**
   * User ID.
   *
   * @var string
   */
  private $userId = NULL;

  /**
   * Password.
   *
   * @var string
   */
  private $empassword = NULL;

  /**
   * medcode.
   *
   * @var string
   */
  private $medcode = NULL;

  /**
   * authorization.
   *
   * @var string
   */
  private $authorisationcode = NULL;

  /**
   * environment.
   *
   * @var string
   */
  private $environment = NULL;
  
   /**
   * Non-prod End point URL.
   *
   * @var string
   */
  private $nonprodEndpointUrl = NULL;
  
  /**
   * Production End point URL.
   *
   * @var string
   */
  private $prodEndpointUrl = NULL;
    
  
  /**
   * Constructor. Should not be really used. Check the static methods.
   */
  public function __construct() {
    try {
      $this->setUserId(variable_get('en_user_id', ''));
      $this->setEmpassword(variable_get('en_password', ''));
      $this->setMedcode(variable_get('en_medcode', ''));           
      $this->setEnvironment(variable_get('en_environment', ''));
      $this->setNonprodEndpointUrl(variable_get('en_test_webservice_end_point', ''));
      $this->setProdEndpointUrl(variable_get('en_prod_webservice_end_point', ''));      
      $this->setApiEndpointUrl();     
      $this->setEngagedmediaAuthToken();      
    } catch (Exception $e) {
      watchdog_exception("epsilon_harmony_engagemedia", $e, $e->getMessage(), array(), WATCHDOG_ERROR);
      drupal_set_message($e->getMessage(), 'error');
    }
  }

  /**
   * Set the authentication username.
   *
   * @param string $username
   *   Username.
   *
   * @throws Exception
   *   Username must be at least 1 character long.
   */
  protected function setUserId($userId) {
    // Check if the Engagedmedia userId is present.
    if (empty($userId) || strlen($userId) < 1) {
      throw new Exception('Engagedmedia UserId is missing.');
    }
    else {
      $this->userId = $userId;
    }
  }

  /**
   * Gets the assigned user id.
   *
   * @return mixed
   *   Returns user id.
   */
  protected function getUserId() {
    return $this->userId;
  }

  /**
   * Set the authentication password.
   *
   * @param string $password
   *   Password.
   *
   * @throws Exception
   *   Password must be at least 1 character long.
   */
  protected function setEmpassword($empassword) {
    // Check if the Engagedmedia password is present.
    if (empty($empassword) || strlen($empassword) < 1) {
      throw new Exception('Engagedmedia Password is missing.');
    }
    else {
      $this->empassword = $empassword;
    }
  }

  /**
   * Gets the assigned password.
   *
   * @return mixed
   *   Returns password.
   */
  protected function getEmpassword() {
    return $this->empassword;
  }

  /**
   * Set the authentication Medcode.
   *
   * @param string $medcode
   *   Medcode.
   *
   * @throws Exception
   *  Medcode must be at least 1 character long.
   */
  protected function setMedcode($medcode) {
    // Check if the Medcode is present.
    if (empty($medcode) || strlen($medcode) < 1) {
      throw new Exception('Engagedmedia medcode is missing.');
    }
    else {
      $this->medcode = $medcode;
    }
  }

  /**
   * Gets the assigned medcode.
   *
   * @return mixed
   *   Returns medcode.
   */
  protected function getMedcode() {
    return $this->medcode;
  } 

  /**
   * Set the environment.
   *
   * @param string $environment
   *   environment.
   *
   * @throws Exception
   *   environment must be at least 1 character long.
   */
  protected function setEnvironment($environment) {
    // Check if the environment is present.
    if (empty($environment) || strlen($environment) < 1) {
      throw new Exception('Environment is missing.');
    }
    else {
      $this->environment = $environment;
    }
  }

  /**
   * Gets the assigned environment.
   *
   * @return mixed
   *   Returns environment.
   */
  protected function getEnvironment() {
    return $this->environment;
  }
  
  /**
   * Set the non-prod Endpoint URL.
   *
   * @param string $nonprodEndpointUrl
   *   nonprodEndpointUrl.
   *
   * @throws Exception
   *   nonprodEndpointUrl must be at least 1 character long.
   */
  protected function setNonprodEndpointUrl($nonprodEndpointUrl) {
    // Check if the environment is present.
    if (empty($nonprodEndpointUrl) || strlen($nonprodEndpointUrl) < 1) {
      throw new Exception('Non-prod Endpoint Url is missing.');
    }
    else {
      $this->nonprodEndpointUrl = $nonprodEndpointUrl;
    }
  }

  /**
   * Gets the assigned non-prod Endpoint URL.
   *
   * @return mixed
   *   Returns npn-prod Endpoint URL.
   */
  protected function getNonprodEndpointUrl() {
    return $this->nonprodEndpointUrl;
  }
  
  
   /**
   * Set the prod Endpoint URL.
   *
   * @param string $prodEndpointUrl
   *   prodEndpointUrl.
   *
   * @throws Exception
   *   prodEndpointUrl must be at least 1 character long.
   */
  protected function setProdEndpointUrl($prodEndpointUrl) {
    // Check if the environment is present.
    if (empty($prodEndpointUrl) || strlen($prodEndpointUrl) < 1) {
      throw new Exception('Prod Endpoint Url is missing.');
    }
    else {
      $this->prodEndpointUrl = $prodEndpointUrl;
    }
  }

  /**
   * Gets the assigned Prod Endpoint URL.
   *
   * @return mixed
   *   Returns Prod Endpoint URL.
   */
  protected function getProdEndpointUrl() {
    return $this->prodEndpointUrl;
  } 
  
  /**
   * Create the base URL for the selected environment.
   */
  protected function setApiEndpointUrl() {
    if($this->environment == "test") {     
      $this->apiUrl = $this->nonprodEndpointUrl;
    }
    else {     
      $this->apiUrl = $this->prodEndpointUrl;
    }
    return $this->apiUrl;
  }  

  /**
   * Concatenate the User id and Password.
   *
   * @return mixed
   *   Returns encoded Base64 value.
   */
  protected function setEngagedmediaAuthToken() {    
    return base64_encode($this->userId . ":" . $this->empassword);
  }
  
}



<?php

/**
 * @file
 * This file contains the admin section configurations form.
 */

/**
 * Implements hook_form().
 */
function en_webservice_config_form($form, &$form_state) {
  
  // Select Environment.
  $form['en_environment'] = array(
    '#type' => 'radios',
    '#title' => t("Select the Environment:"),
    '#options' => array(
      "test" => t("Test"),
      "prod" => t("Production"),
    ),
    '#default_value' => variable_get('en_environment'),
    '#required' => TRUE,
  );
  
  $form['en_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Engagedmedia User ID'),
    '#description' => t('User Id for engagedmedia.'),
    '#default_value' => variable_get('en_user_id', ''),
    '#required' => TRUE,
  );

  $form['en_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Engagedmedia Password'),
    '#description' => t('Password for engagedmedia.'),
    '#default_value' => variable_get('en_password', ''),
    '#required' => TRUE,
  );

  $form['en_medcode'] = array(
    '#type' => 'textfield',
    '#title' => t('MedCode'),
    '#description' => t('medCode for Engagedmedia.'),
    '#default_value' => variable_get('en_medcode', ''),
    '#required' => TRUE,
  );

  $form['en_test_webservice_end_point'] = array(
    '#type' => 'textfield',
    '#title' => t('Non-Prod Engagedmedia Websevice Endpoint URL'),
    '#description' => t('Enter the endpoint URL for calling engagedmedia API in Test site. Enter full URL without query string.'),
    '#default_value' => variable_get('en_test_webservice_end_point', ''),
    '#required' => TRUE,
  );
  
  $form['en_prod_webservice_end_point'] = array(
    '#type' => 'textfield',
    '#title' => t('Prod Engagedmedia Websevice Endpoint URL'),
    '#description' => t('Enter the endpoint URL for calling engagedmedia API in Live site. Enter full URL without query string.'),
    '#default_value' => variable_get('en_prod_webservice_end_point', ''),
    '#required' => TRUE,
  );  
  return system_settings_form($form);
}

/**
 * Implements hook_form() - Responsible for the webform configuration tab.
 */
function en_webform_config_form($form, &$form_state) {
  module_load_include('inc', 'epsilon_harmony_engagemedia', 'includes/epsilon_harmony_engagedmedia.utils');

  $form = array();
  //$form['#validate'][] = 'walleturl_webform_config_validate';

  $available_webforms = ep_en_get_available_webforms();

  if (empty($available_webforms)) {
    // If does not exist any available webform, a warning message will be displayed.
    $text_message = 'Does not exist any Webform. Please create at least one to configure the Engagedmedia SHORT URL.';
    $type = 'warning';
    drupal_set_message(t($text_message), $type, FALSE);
    return system_settings_form($form);
  }

  foreach ($available_webforms as $key => $value) {
    $form['digital_wallet_url_webform']['wallet_url_integration_enabled_' . $value->nid] = array(
      '#type' => 'fieldset',
      '#title' => $value->title . ' [nid: ' . $value->nid . ']',
      '#weight' => $value->nid,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['digital_wallet_url_webform']['wallet_url_integration_enabled_' . $value->nid]['wallet_url_integration_enabled_' . $value->nid] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Engagedmedia integration'),
      '#default_value' => variable_get('wallet_url_integration_enabled_' . $value->nid, FALSE),
    );    
  }

  return system_settings_form($form);
}

function ep_en_enabled_webforms_config_form($form, &$form_state) {
  
  module_load_include('inc', 'epsilon_harmony_engagemedia', 'includes/epsilon_harmony_engagedmedia.utils');

  $form = array();
  //$form['#validate'][] = 'pfe_epsilon_emails_templates_validate';

  $available_webforms = ep_en_get_available_webforms();

  if (empty($available_webforms)) {
    // If does not exist any available webform, a warning message will be displayed.
    $text_message = 'Does not exist any Webform. Please create at least one to configure the dynamic parameters.';
    $type = 'warning';
    drupal_set_message(t($text_message), $type, FALSE);
    return system_settings_form($form);
  }

  $enabled_webforms = ep_en_get_enabled_webforms();

  if (empty($enabled_webforms)) {
    // If exists at least one webform but no configuration has been set, a warning message will be displayed.
    $text_message = '<a href =' . base_path() . 'admin/config/services/ep-en/webform-settings> Click here </a> to configure the webform(s) before setting up the dynamic parameters.';
    $type = 'warning';
    drupal_set_message(t($text_message), $type, FALSE);
    return system_settings_form($form);
  }

  foreach ($enabled_webforms as $key => $value) {
    $templates_count = variable_get('epsilon_integration_emails_count_' . $key);
    $webform_title = 'Webform ' . $value;

    $form['digital_wallet_webform_conf_' . $key] = array(
      '#type' => 'fieldset',
      '#title' => t($webform_title),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['digital_wallet_webform_conf_' . $key]['digital_wallet_webform_dynamic_parameters_' . $key] = array(
      '#type' => 'textarea',
      '#title' => t('Dynamic Parameters For Epsilon Harmony'),
      '#default_value' => variable_get('digital_wallet_webform_dynamic_parameters_' . $key),      
      '#required' => TRUE,
      '#description' => t('<strong>Key-value pairs MUST be specified as "Harmony DYNAMIC PARAMETERS|Numeric value chronologically starting from 1"</strong>. Use of only alphanumeric characters and underscores is recommended in keys. Strictly one option per line.'),
    );
    
    $form['digital_wallet_webform_conf_' . $key]['digital_wallet_webform_harmoney_key_' . $key] = array(
        '#type' => 'textfield',
        '#title' => t('Epsilon Harmony Message Identifier'),
        '#default_value' => variable_get('digital_wallet_webform_harmoney_key_' . $key),
        '#size' => 50,
        '#required' => TRUE,
        '#description' => t('Specify the Harmony message identifier that needs to be set for this webform.<a href =' . base_path() . 'admin/config/services/epsilon_harmony/message_configuration> Click here </a> to get the value.'),
    );    
    
  }

  return system_settings_form($form);
}

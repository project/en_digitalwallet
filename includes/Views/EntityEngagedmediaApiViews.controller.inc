<?php

/**
 * Entity Views Controller class.
 */
class EntityEpEnLogsViewsController extends EntityDefaultViewsController {

  /**
   * Edit or add extra fields to views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    $data['en_digitalwallet_logs']['created']['field']['handler'] = 'views_handler_field_date';
    $data['en_digitalwallet_logs']['uid'] = array(
      'title' => t('User'),
      'help' => t('Relate a user who created it.'),
      'relationship' => array(
        'handler' => 'views_handler_relationship',
        'base' => 'users',
        'base field' => 'uid',
        'field' => 'uid',
        'field_name' => 'uid',
        'label' => t('user'),
      ),
    );
    return $data;
  }

}

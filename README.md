﻿
# Engagedmedia Digital Wallet Documentation

Follow the below procedure to install and use the  module for Drupal 7.

## Installation

 - Download the module from https://www.drupal.org/project/en_digitalwallet
 - Copy the module content in `/sites/all/modules` directory in your Drupal installation.
 - Enable the module from `/admin/modules`.

## Configuration

 - Go to `admin/config/services/ep-en` and fill in all the required fields.


## Usage

In order to use the module, just pass the data to desired method, more of this is explained below.
##### Please use the following code to call the API.
```
<?php
	use use Drupal\epsilon_harmony_engagemedia\EnApiFactory;;
	$classObj = new EnApiFactory; 
?>
```

### Method:
To create a record pass the desired attributes in a key=>value pair.
```
<?php
	
?>


```
## Logging
All the API triggers are logged in `admin/config/services/ep-en/logs` which can be used for debugging purpose.
